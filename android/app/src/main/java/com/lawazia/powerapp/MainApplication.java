package com.lawazia.powerapp;

import android.app.Application;

//import com.RNFetchBlob.RNFetchBlobPackage;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookSdk;
//import com.facebook.appevents.AppEventsLogger;
import com.facebook.react.ReactApplication;
import com.lawazia.powerapp.react.PowerAppPackage;
import com.reactlibrary.RNGooglePlacePickerPackage;
//import com.rnfs.RNFSPackage;
//import com.mybigday.rns3.*;
import io.sentry.RNSentryPackage;
//import com.reactlibrary.veditor.RNVideoEditorPackage;
//import com.github.yamill.orientation.OrientationPackage;
import io.branch.rnbranch.RNBranchPackage;
import com.airbnb.android.react.lottie.LottiePackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.microsoft.codepush.react.CodePush;
//import com.gettipsi.stripe.StripeReactPackage;
//import com.reactlibrary.thumbnail.RNThumbnailPackage;
//import com.brentvatne.react.ReactVideoPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.lwansbrough.RCTCamera.RCTCameraPackage;
//import com.facebook.reactnative.androidsdk.FBSDKPackage;
//import com.BV.LinearGradient.LinearGradientPackage;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
//import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.instabug.reactlibrary.RNInstabugReactnativePackage;
import com.razorpay.rn.RazorpayPackage;
//import fr.bamlab.rnimageresizer.ImageResizerPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
//import com.imagepicker.ImagePickerPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import cl.json.RNSharePackage;
import cl.json.ShareApplication;
//import io.branch.referral.Branch;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage;
import io.invertase.firebase.auth.RNFirebaseAuthPackage;
import io.invertase.firebase.config.RNFirebaseRemoteConfigPackage;
import io.invertase.firebase.perf.RNFirebasePerformancePackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ShareApplication, ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

    @Override
    protected String getJSBundleFile() {
      return CodePush.getJSBundleFile();
    }
    
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNGooglePlacePickerPackage(),
            //new RNFSPackage(),
            //new RNVideoEditorPackage(),
            //new OrientationPackage(),
            new RNBranchPackage(),
            new RNGoogleSigninPackage(),
            new CodePush(getString(R.string.CodePushDeploymentKey), getApplicationContext(), BuildConfig.DEBUG),
            //new StripeReactPackage(),
            //new RNThumbnailPackage(),
            //new ReactVideoPackage(),
            new MapsPackage(),
            //new RNS3Package(),
            //new RNFetchBlobPackage(),
            new VectorIconsPackage(),
            new RCTCameraPackage(),
            new LottiePackage(),
            //new ReactNativeLocalizationPackage(),
            //new LinearGradientPackage(),
            //new FBSDKPackage(mCallbackManager),
            new ReactNativeOneSignalPackage(),
            new RazorpayPackage(),
            new RNSharePackage(),
            //new ImagePickerPackage(),
            new PickerPackage(),
            //new ImageResizerPackage(),
            new FastImageViewPackage(),
            new PowerAppPackage(),
            new RNFirebasePackage(),
            new RNFirebaseAnalyticsPackage(),
            new RNFirebaseAuthPackage(),
            new RNFirebaseRemoteConfigPackage(),
            new RNFirebasePerformancePackage(),
            new RNSentryPackage(),
            new RNInstabugReactnativePackage.Builder(getString(R.string.instabug_token), MainApplication.this)
                    .setInvocationEvent("shake")
                    .setPrimaryColor("#1D82DC")
                    .setFloatingEdge("left")
                    .setFloatingButtonOffsetFromTop(250)
                    .build()

      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  /*
  private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

  protected static CallbackManager getCallbackManager() {
    return mCallbackManager;
  }
  */

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    //Branch.getAutoInstance(this);
    //FacebookSdk.sdkInitialize(getApplicationContext());
    //AppEventsLogger.activateApp(this);
  }

  @Override
  public String getFileProviderAuthority() {
    return "com.lawazia.urbanstall.provider";
  }


}
