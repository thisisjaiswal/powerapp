package com.lawazia.powerapp.react;

import android.app.Activity;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.lawazia.powerapp.R;


import java.lang.ref.WeakReference;

public class SplashScreenModule extends ReactContextBaseJavaModule{


    public SplashScreenModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "SplashScreen";
    }

    @ReactMethod
    public void hide() {
        Activity activity = getCurrentActivity();
        if (activity == null) return;
        final Activity _activity = activity;
        _activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _activity.setTheme(R.style.AppTheme);
            }
        });
    }
}