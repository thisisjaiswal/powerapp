require 'nokogiri'

module Fastlane
  module Actions
    class AndroidAppmetaAction < Action
      def self.run(params)               
       
        config = JSON.parse(File.read(params[:target_app_path] + '/config.json'))
         
        #update strings.xml
        stringsFile = "app/src/main/res/values/strings.xml"
        encoding = "UTF-8"
        doc = File.open(stringsFile, "r:#{encoding}") { |f|
          @doc = Nokogiri::XML(f)

          @doc.css("resources string[@name=app_name]").each do |response_node|
            response_node.content = config['appName']
            UI.message("Updating app name to: #{config['appName']}")
          end

          @doc.css("resources string[@name=CodePushDeploymentKey]").each do |response_node|
            response_node.content = config['codepushKey']
            UI.message("Updating codePushKey to: #{config['codepushKey']}")
          end

          @doc.css("resources string[@name=branch_key]").each do |response_node|
            response_node.content = config['branchkey']
            UI.message("Updating branchkey to: #{config['branchkey']}")
          end

          @doc.css("resources string[@name=branch_scheme]").each do |response_node|
            response_node.content = config['branchScheme']
            UI.message("Updating branchScheme to: #{config['branchScheme']}")
          end

          @doc.css("resources string[@name=branch_app_domain]").each do |response_node|
            response_node.content = config['branchAppDomain']
            UI.message("Updating branchAppDomain to: #{config['branchAppDomain']}")
          end

          @doc.css("resources string[@name=branch_alt_app_domain]").each do |response_node|
            response_node.content = config['branchAltAppDomain']
            UI.message("Updating branchAltAppDomain to: #{config['branchAltAppDomain']}")
          end

          @doc.css("resources string[@name=facebook_app_id]").each do |response_node|
            response_node.content = config['facebookAppId']
            UI.message("Updating facebookAppId to: #{config['facebookAppId']}")
          end

          @doc.css("resources string[@name=fb_login_protocol_scheme]").each do |response_node|
            response_node.content = config['facebookUrlScheme']
            UI.message("Updating facebookUrlScheme to: #{config['facebookUrlScheme']}")
          end

          @doc.css("resources string[@name=gmap_api_key]").each do |response_node|
            response_node.content = config['googleApiKey']
            UI.message("Updating googleApiKey to: #{config['googleApiKey']}")
          end          

          File.write(stringsFile, @doc.to_xml(encoding: encoding))
        }
        
        #update build.gradle
        data =  ''
        gradleFile = "build.gradle"
        File.open(gradleFile, 'r') do |file|
          file.each_line do |line|
            if line.include? "applicationId"
              data += "    applicationId = '#{config['applicationId']}'\n"
              UI.message("Updating applicationId to: #{config['applicationId']}")
            elsif line.include? "versionName"
              data += "    versionName = '#{config['versionName']}'\n"
              UI.message("Updating versionName to: #{config['versionName']}")
            elsif line.include? "versionCode"
              data += "    versionCode = #{config['versionCode']}\n"
              UI.message("Updating versionCode to: #{config['versionCode']}")
            elsif line.include? "storeFile"
              storeFilePath = "#{params[:target_app_path]}/sign/#{config['storeFile']}.jsk"
              data += "    storeFile = '#{storeFilePath}'\n"
              UI.message("Updating storeFile to: #{storeFilePath}")
            elsif line.include? "storePassword"
              data += "    storePassword = '#{config['storePassword']}'\n"
              UI.message("Updating storePassword to: *********")
            elsif line.include? "keyAlias"
              data += "    keyAlias = '#{config['keyAlias']}'\n"
              UI.message("Updating keyAlias to: #{config['keyAlias']}")
            elsif line.include? "keyPassword"
              data += "    keyPassword = '#{config['keyPassword']}'\n"
              UI.message("Updating keyPassword to: *********")
            else
              data += line
            end
          end          
          file.close          
          File.write(gradleFile, data)          
        end
 
      end

      def self.available_options
        # Define all options your action supports. 
        
        # Below a few examples
        [          
          FastlaneCore::ConfigItem.new(key: :target_app_path,
                                      env_name: "ANDROIDAPPMETA_TARGET_APP_PATH",
                                   description: "Target App Path",
                                      optional: false,
                                          type: String,
                                 default_value: "")
        ]
      end

      def self.output
        # Define the shared values you are going to provide
        # Example
        [
          ['UPDATE_APP_META_CUSTOM_VALUE', 'A description of what this value contains']
        ]
      end

      def self.return_value
        # If your method provides a return value, you can describe here what it does
      end

      def self.is_supported?(platform)
        platform == :android
      end
    end
  end
end
