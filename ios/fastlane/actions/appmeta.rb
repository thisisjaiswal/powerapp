

module Fastlane
  module Actions
    class AppmetaAction < Action
      def self.run(params)

        config = JSON.parse(File.read(params[:target_app_path] + '/config.json'))


       
      end

      def self.available_options
        # Define all options your action supports. 
        
        # Below a few examples
        [
          FastlaneCore::ConfigItem.new(key: :target_app_path,
                                      env_name: "APPMETA_TARGET_APP_PATH",
                                   description: "Target App Path",
                                      optional: false,
                                          type: String,
                                 default_value: "")
        ]
      end

      def self.output
        # Define the shared values you are going to provide
        # Example
        [
          ['APPMETA_CUSTOM_VALUE', 'A description of what this value contains']
        ]
      end

      def self.return_value
        # If your method provides a return value, you can describe here what it does
      end

      def self.is_supported?(platform)
        platform == :ios
      end
    end
  end
end
